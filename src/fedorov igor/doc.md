# ������ ���������������� 2: ����������������� ������� �� ��������


## ���� � ������


###���� ������ ������ 
� ������ ������������ ������ �������� ������ �������� ����������� �������, �������������� ����������� �������� ������ ������������ ���� (�����������������) � ���������� �������� �������� ��� ����:

- ��������/���������;
- �����������;
- ���������.

� �������� ���������� ������������ ������ ��������� ������������ ������� �������� ������ Git � ��������� ��� ���������� �������������� ������ _Google Test_.

###������:    
- ���������� ������� ���������� ������ `TVector` �������� ��������� ����������.
- ���������� ������� ���������� ������ `TMatrix` �������� ��������� ����������.
- ���������� ��������� ������, ����������� ��� ������ ������� `TVector` � `TMatrix`.
- ����������� ����������������� ������ � ������� �������������.
- ����������� ������� ������������� � �������� ����������, ����������� �������� ������� � ������������ �������� �������� ��� ����.

## ������ ������:

###����:
- ���������� ������� ������ � ������� (`TVector` � `TMatrix` � _utmatrix.h_)
- ��������� ����� ������� ������ ��� ������� �� ��������� �������.
- ����� ��������� ������ ��� ������� �� ��������� �������.
- �������� ������ ������������� ������ �������

������ (`TMatrix` ��������� ����� `TVector`) ����������� ��� �������, ������� ���������� � ���������� ������� ��������� � ����� �����.
��������� �������������� ������� ����������� ��������:
```
const int MAX_VECTOR_SIZE = 100000000; // ������������ ������ �������
const int MAX_MATRIX_SIZE = 10000; // ������������ ������ �������
```

### 1. ������ ������ TVector:

#### ����������:
```
// ������ �������
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       // ������ �������
  int StartIndex; // ������ ������� �������� �������
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);                // ����������� �����������
  ~TVector();
  int GetSize()      { return Size;       } // ������ �������
  int GetStartIndex(){ return StartIndex; } // ������ ������� ��������
  ValType& operator[](int pos);             // ������
  bool operator==(const TVector &v) const;  // ���������
  bool operator!=(const TVector &v) const;  // ���������
  TVector& operator=(const TVector &v);     // ������������

  // ��������� ��������
  TVector  operator+(const ValType &val);   // ��������� ������
  TVector  operator-(const ValType &val);   // ������� ������
  TVector  operator*(const ValType &val);   // �������� �� ������

  // ��������� ��������
  TVector  operator+(const TVector &v);     // ��������
  TVector  operator-(const TVector &v);     // ���������
  ValType  operator*(const TVector &v);     // ��������� ������������

  // ����-�����
  friend istream& operator>>(istream &in, TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      in >> v.pVector[i];
    return in;
  }
  friend ostream& operator<<(ostream &out, const TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      out << v.pVector[i] << ' ';
    return out;
  }
};
```

#### ���������� �������:
```
template <class ValType>
TVector<ValType>::TVector(int s, int si)
{
	if (s < 1 || si < 0)
		throw "error in constructer";
	if (s > MAX_VECTOR_SIZE)
		throw "error. too large size or index";
	Size = s;
	StartIndex = si;
	pVector = new ValType[Size];
	memset(pVector, 0, Size * sizeof(ValType));
} /*-------------------------------------------------------------------------*/

template <class ValType> //����������� �����������
TVector<ValType>::TVector(const TVector<ValType> &v)
{
	if (Size != v.Size){ //��������, ������������ ����� �� �������� ������ ������.
		pVector = new ValType[v.Size];
		for (int i = 0; i < v.Size; i++)
			pVector[i] = v.pVector[i];
		Size = v.Size;
		StartIndex = v.StartIndex;
	}
	else{
		StartIndex = v.StartIndex;
		Size = v.Size;
		for (int i = 0; i < v.Size; i++)
			pVector[i] = v.pVector[i];
	}
} 
/*-------------------------------------------------------------------------*/

template <class ValType>
TVector<ValType>::~TVector(){
	delete[] pVector;
} 
/*-------------------------------------------------------------------------*/

template <class ValType> // ������
ValType& TVector<ValType>::operator[](int pos)
{
	if ((pos - StartIndex) < 0 || (pos - StartIndex) >= Size) // �������� ������ �������
		throw("error. element doest exist");
	else
		return pVector[pos - StartIndex];
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TVector<ValType>::operator==(const TVector &v) const{
	if (v.Size != this->Size)
		return false;
	else
		for (int i = 0; i < v.Size;i++)
			if(v.pVector[i] != this->pVector[i])
				return false; 
	return true;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TVector<ValType>::operator!=(const TVector &v) const
{
	if (v == (*this))
		return false;
	else
		return true;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������������
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if (this != &v) {
		if (Size != v.Size)
			pVector = new ValType[v.Size];
		Size = v.Size;
		StartIndex = v.StartIndex;
		for (int i = 0; i < Size; i++)
			pVector[i] = v.pVector[i];
	}
	else
		throw("error. obj adress's is equal");
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������� ������
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	for (int i = 0; i < Size; i++)
		this->pVector[i] = pVector[i] + val;
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������� ������
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	for (int i = 0; i < Size; i++)
		this->pVector[i] = pVector[i] - val;
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // �������� �� ������
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	for (int i = 0; i < Size; i++)
		this->pVector[i] = pVector[i] * val;
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if (this->Size != v.Size)
		throw("error");
	TVector<ValType> temp(Size, StartIndex);
	for (int i = 0; i < Size; i++)
		temp.pVector[i] = pVector[i] + v.pVector[i];
	return temp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if (this->Size != v.Size)
		throw("error");
	TVector temp(Size, StartIndex);
	for (int i = 0; i < Size; i++)
		temp.pVector[i] = pVector[i] - v.pVector[i];
	return temp;
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������� ������������
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	ValType result;
	if (Size != v.Size) 
		throw "error. size not match";
	for (int i = 0; i < Size; i++)
		result += this->pVector[i] * v.pVector[i];
	return result;
} /*-------------------------------------------------------------------------*/
```

### 2. ������ ������ TMatrix:

#### ����������:

```
// ����������������� �������
template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
public:
  TMatrix(int s = 10);                           
  TMatrix(const TMatrix &mt);                    // �����������
  TMatrix(const TVector<TVector<ValType> > &mt); // �������������� ����
  bool operator==(const TMatrix &mt) const;      // ���������
  bool operator!=(const TMatrix &mt) const;      // ���������
  TMatrix& operator= (const TMatrix &mt);        // ������������
  TMatrix  operator+ (const TMatrix &mt);        // ��������
  TMatrix  operator- (const TMatrix &mt);        // ���������

  // ���� / �����
  friend istream& operator>>(istream &in, TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      in >> mt.pVector[i];
    return in;
  }
  friend ostream & operator<<( ostream &out, const TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      out << mt.pVector[i] << endl;
    return out;
  }
};
```

#### ���������� �������:
```
template <class ValType>
TMatrix<ValType>::TMatrix(int s): TVector<TVector<ValType> >(s)
{
	if (s <= 0 || s > MAX_MATRIX_SIZE) 
		throw("too large matrix, sorry");	
	for (int i = 0; i < s; i++)
		pVector[i] = TVector<ValType>(s - i, i);
} /*-------------------------------------------------------------------------*/

template <class ValType> // ����������� �����������
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt):
  TVector<TVector<ValType> >(mt){}

template <class ValType> // ����������� �������������� ����
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt):
  TVector<TVector<ValType> >(mt){}

template <class ValType> // ���������
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const
{	
	return TVector<TVector<ValType> >::operator==(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator!=(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // ������������
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt)
{
	 return TVector<TVector<ValType>>::operator=(mt);
	 /*
	 if (this != &mt) {
		 if (Size != mt.Size)
			 pVector = new TVector<ValType>[mt.Size];
		 Size = mt.Size;
		 StartIndex = mt.StartIndex;
		 for (int i = 0; i < Size; i++)
			 pVector[i] = mt.pVector[i];
	 }
	 else throw"error. obj have equal memory";
	 return *this;*/
} /*-------------------------------------------------------------------------*/

template <class ValType> // ��������
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator+(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // ���������
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType>>::operator-(mt);
}/*-------------------------------------------------------------------------*/
```


### 3. ���������� ������:

� ������ ������ ������������ ��������� ��� ��������� �������������� ������ __Google Test__.

#### 3.1 ����� ��� ������ __TVector__:

```
#include "utmatrix.h"
#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
	TVector<int> v1(10);
	for (int i = 0; i < 10; i++)
		v1[i] = i;
	TVector<int> v2(v1);

	EXPECT_EQ(v2, v1);

}

TEST(TVector, copied_vector_has_its_own_memory)
{
	TVector<int> v1(10);
	TVector<int> v2(v1);
	
	v1[0] = 1;

	EXPECT_NE(v1, v2);
}

TEST(TVector, can_get_size)
{
  TVector<int> v(10);

  EXPECT_EQ(10, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
  TVector<int> v(10, 1);

  EXPECT_EQ(1, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
  TVector<int> v(10);
  v[0] = 10;

  EXPECT_EQ(10, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	ASSERT_ANY_THROW(TVector<int> v(10,-10));
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(10);
	ASSERT_ANY_THROW(v[100] = 0);
}

TEST(TVector, can_assign_vector_to_itself)
{
  TVector<int> v(10);
  ASSERT_ANY_THROW(v = v);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	TVector<int> v(10),v1(10);
	for (int i = 0; i < 10; i++)
		v[i] = i;
	v = v1;
	for (int i = 0; i < 10;i++)
		EXPECT_EQ(v[i], v1[i]);
}

TEST(TVector, assign_operator_change_vector_size)
{

	TVector<int> v1(10), v2(100);
	v1 = v2;

	EXPECT_NE(10, v1.GetSize());
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	TVector<int> v1(10), v2(100);
	v1 = v2;

	EXPECT_NO_THROW(v1=v2);
}

TEST(TVector, compare_equal_vectors_return_true)
{
	TVector<int> v1(10), v2(10);
	v1 = v2;

	EXPECT_TRUE(v1 == v2);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	TVector<int> v1(10);
	EXPECT_TRUE(v1 == v1);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	TVector<int> v1(100), v2(10);
	EXPECT_FALSE(v1 == v2);
}

TEST(TVector, can_add_scalar_to_vector)
{
	TVector<int> v1(10);
	ASSERT_NO_THROW(v1 = v1 + 10);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
	TVector<int> v1(10);
	ASSERT_NO_THROW(v1 = v1 - 10);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	TVector<int> v1(10);
	ASSERT_NO_THROW(v1 = v1 * 10);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	TVector<int> v1(10),v2(10);
	for (int i = 0; i < 10; i++)
		v1[i] = i;
	ASSERT_NO_THROW(v1 = v1 + v2);
	ASSERT_EQ(v1 ,v2 = v1 + v2);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	TVector<int> v1(10), v2(2);
	v2[0] = 10;
	v2[1] = 10;
	ASSERT_ANY_THROW(v1 = v1 + v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	TVector<int> v1(10),v2(10),v3(10);
	for (int i = 0; i < 10; i++){
		v1[i] = i;
		v2[i] = i;
	}
	v1 = v1 - v2;
	ASSERT_TRUE(v1 == v3);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	TVector<int> v1(100), v2(10);
	ASSERT_ANY_THROW(v2 = v2 - v1);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	TVector<int> v1(10), v2(10);
	for (int i = 0; i < 10; i++){
		v1[i] = i;
		v2[i] = 1;
	}
	EXPECT_EQ(45, v1 * v2);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	TVector<int> v1(100), v2(10);
	ASSERT_ANY_THROW(v2 = v2 * v1);
}

```
#### 3.2 ����� ��� ������ __TMatrix__:

```

#include "gtest.h"
#include "utmatrix.h"

TEST(TMatrix, can_create_matrix_with_positive_length)
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	TMatrix<int> matr1(1), matr2(matr1);
	EXPECT_EQ(matr1, matr2);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	TMatrix<int> matr1(1);
	matr1[0][0] = 1;
	TMatrix<int> matr2(matr1);
	matr1[0][0] = 0;
	EXPECT_EQ(1, matr2[0][0]);
}

TEST(TMatrix, can_get_size)
{
	TMatrix <int> matr(10);
	EXPECT_EQ(10, matr.GetSize());;
}

TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<int> matr(10);
	matr[0][0] = 10;
	EXPECT_EQ(10, matr[0][0]);
	
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> matr(10);
	EXPECT_ANY_THROW(matr[-1][-1] = 10);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	TMatrix<int> matr(10);
	EXPECT_ANY_THROW(matr[100][100] = 2);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
	TMatrix<int> matr1(10);
	EXPECT_ANY_THROW(matr1=matr1);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	TMatrix<int> matr1(10),matr2(10);
	EXPECT_NO_THROW(matr1 = matr2);
}

TEST(TMatrix, assign_operator_change_matrix_size)
{

}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
	TMatrix<int> matr1(10),matr2(11);
	EXPECT_NO_THROW(matr1 = matr2);
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
	TMatrix<int> matr1(10), matr2(10);
	EXPECT_TRUE(matr1 == matr2);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	TMatrix<int> matr1(10);
	EXPECT_TRUE(matr1 == matr1);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	TMatrix<int> matr1(10), matr2(11);
	EXPECT_FALSE(matr1 == matr2);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
	TMatrix<int> matr1(10), matr2(10), matr3(10);
	matr1[0][0] = 1;
	matr2[1][1] = 1;

	matr3[0][0] = 1;
	matr3[1][1] = 1;

	matr2 = matr2 + matr1;
	EXPECT_TRUE(matr3 == matr2);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	TMatrix<int> matr1(10), matr2(11);
	EXPECT_ANY_THROW(matr1 = matr1 + matr2);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{	
	TMatrix<int> m1(10), m2(10), m3(10);
	for (int i = 0; i < 10; i++) {
		m1[i][i] = i;
		m2[i][i] = i;
		m3[i][i] = 0;
	}
	EXPECT_EQ(m1 - m2, m3);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	TMatrix<int> matr1(10), matr2(11);
	EXPECT_ANY_THROW(matr1 = matr1 - matr2);

}


```

### 4. ����������������� ������:

![](http://s61.radikal.ru/i174/1611/ab/f83aa03c087f.png)
![](http://s018.radikal.ru/i507/1611/65/afbdb1ef7a0c.png)
![](http://s41.radikal.ru/i094/1611/36/8b0c54b1942d.png)

��� ����� ��������.

### 5. ����������� ������� �������������:

#### �������� ��� ��������� ����������:

```
#include <iostream>
#include "utmatrix.h"

//---------------------------------------------------------------------------

void main()
{
  TMatrix<int> a(5), b(5), c(5);
  int i, j;

  setlocale(LC_ALL, "Russian");
  cout << "������������ �������� ��������� ������������� ����������� ������"
    << endl;
  for (i = 0; i < 5; i++)
    for (j = i; j < 5; j++)
    {
      a[i][j] =  i * 10 + j;
      b[i][j] = (i * 10 + j) * 100;
    }

  c = a + b;//���������� ������� � ������������ 

  cout << "Matrix a = " << endl << a << endl;
  cout << "Matrix b = " << endl << b << endl;
  cout << "Matrix c = a + b" << endl << c << endl;
  
  if (a == b) //���������� ���������
	  cout << "Matrix a and b is not equal" << endl << endl;
  else
	  cout << "Matrix a and b is equal" << endl << endl;

  c = b - a; // ���������� ��������

  cout << "Subtract Matrix a and b" << endl << c << endl;

  c = a; //���������� ������������

  cout << "Assign Matrix a to c" << endl << c << endl;

  cout << "enter Matrix c " << endl;

  cin >> c; // ����������� �����
  cout << c << endl;
}
```
### 4. ���������� ������:

![](http://s012.radikal.ru/i320/1611/ec/d4902463e280.png)


# �����:

� ������ ������ ���� ����������� ��� ������:
- `TVector` ��� ������ � ���������.
- `TMatrix` ��� ������ � ������������������ ���������. 

� ���� ������ ���� �������� ������������������� ��� ���� ������������ � �������� �������� �++. 
�� ������ __TVector__ ���������� __TMatrix__ ��� ������ ������������ �� �������� ���������� ����� ����.
� ������� ��������� �� �������� �� ���� ������, ��� ��������� �� ������ ��������� ���������� ��� ������ �����, � ��� �������� �����.� �������.
����� ��� ������� � ������� ���� �������� ����� ��� ������ Google test. ������ ����� ����� ������ ��� �������� �������� ������ ����. 
��� ��������� ������ ������ ������������ ��������� � ��. ���������� ����������� � ����, � ��� ������������ � ���������������. 
 